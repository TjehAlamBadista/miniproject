package com.example.authfirebase.ui.login

object LoginValidator {

    private val existsingUser = listOf("Alambadista@gmail.com", "12345678")

    /**
     * Email dan Password tidak boleh kosong
     * Email harus valid
     * password harus lebih dari 8 karakter
     */

    fun inputValidator(
        email: String,
        password: String
    ) : Boolean{
        if (email.isEmpty() || password.isEmpty()){
            return false
        }
        if (email in existsingUser && password in existsingUser){
            return true
        }
        if (!email.equals(existsingUser)){
            return false
        }
        if(!password.equals(existsingUser)){
            return false
        }
        if (password.length < 8 ){
            return false
        }
        return true
    }
}