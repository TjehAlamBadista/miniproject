package com.example.authfirebase.ui.note.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.authfirebase.util.Event
import com.example.authfirebase.util.Resource
import com.example.authfirebase.data.note.NoteDatabase
import com.example.authfirebase.data.note.NoteEntity
import com.example.authfirebase.repository.NoteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NoteViewModel(application: Application): AndroidViewModel(application) {
    val allNotes: LiveData<List<NoteEntity>>
    val repository: NoteRepository

    init {
        val dao = NoteDatabase.getDatabase(application).getNotesDao()
        repository = NoteRepository(dao)
        allNotes = repository.allNotes
    }

    private val _InsertNoteItemStatus = MutableLiveData<Event<Resource<NoteEntity>>>()
    val insertNoteItemStatus : LiveData<Event<Resource<NoteEntity>>> = _InsertNoteItemStatus

    fun deleteNote(note: NoteEntity) = viewModelScope.launch(Dispatchers.IO){
        repository.delete(note)
    }

    fun updateNote(note: NoteEntity) = viewModelScope.launch(Dispatchers.IO){
        repository.update(note)
    }

    fun addNote(note: NoteEntity) = viewModelScope.launch(Dispatchers.IO){
        repository.insert(note)
    }

    fun insertNote(title: String, description: String){
        if (title.isEmpty() || description.isEmpty()){
            _InsertNoteItemStatus.postValue(Event(Resource.error("File Tidak Boleh Kosong", null)))
            return
        }
        if (title.length < 8){
            _InsertNoteItemStatus.postValue(Event(Resource.error("Title Harus Lebih Dari 8 Karakter", null)))
            return
        }
        if (description.length < 8){
            _InsertNoteItemStatus.postValue(Event(Resource.error("Deskripsi Harus Lebih Dari 8 Karakter", null)))
            return
        }

    }
}