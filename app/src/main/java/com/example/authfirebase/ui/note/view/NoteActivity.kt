package com.example.authfirebase.ui.note.view

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.authfirebase.R
import com.example.authfirebase.data.note.NoteEntity
import com.example.authfirebase.ui.note.adapter.NoteAdapter
import com.example.authfirebase.ui.note.adapter.NoteClickDeleteInterface
import com.example.authfirebase.ui.note.adapter.NoteClickInterface
import com.example.authfirebase.ui.note.viewmodel.NoteViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton


class NoteActivity : AppCompatActivity(), NoteClickInterface, NoteClickDeleteInterface {

    lateinit var notesRV: RecyclerView
    lateinit var addFAB: FloatingActionButton
    lateinit var viewModal: NoteViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note)

        supportActionBar?.hide()

        notesRV = findViewById(R.id.notesRV)
        addFAB = findViewById(R.id.btn_addFAB)
        notesRV.layoutManager = LinearLayoutManager(this)

        val noteAdapter = NoteAdapter(this, this, this)
        notesRV.adapter = noteAdapter
        viewModal = ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(application))
            .get(NoteViewModel::class.java)
        viewModal.allNotes.observe(this, Observer { list ->
            list?.let {
                noteAdapter.updateList(it)
            }
        })

        addFAB.setOnClickListener {
            Intent(this@NoteActivity, AddEditNoteActivity::class.java).also {
                startActivity(it)
                this.finish()
            }
        }
    }

    override fun onNoteClick(note: NoteEntity) {
        val intent = Intent(this@NoteActivity, AddEditNoteActivity::class.java)
        intent.putExtra("noteType", "Edit")
        intent.putExtra("noteTitle", note.noteTitle)
        intent.putExtra("noteDescription", note.noteDescription)
        intent.putExtra("noteId", note.id)
        startActivity(intent)
        this.finish()
    }

    override fun onDeleteIconClick(note: NoteEntity) {
        viewModal.deleteNote(note)
        Toast.makeText(this, "${note.noteTitle} Deleted", Toast.LENGTH_LONG).show()
    }
}