package com.example.authfirebase.ui.movie.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieResponse(
    val results : List<Movie>

) : Parcelable {
    constructor() : this(mutableListOf())
}