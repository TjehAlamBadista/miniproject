package com.example.authfirebase.ui.movie

import com.example.authfirebase.ui.movie.model.Movie

object MovieValidator {
    fun validateMovie(movie: Movie) : Boolean {
        if (movie.id!!.isNotEmpty() && movie.title!!.isNotEmpty() && movie.poster_path!!.isNotEmpty() && movie.release_date!!.isNotEmpty()){
            return true
        }
        return false
    }
}