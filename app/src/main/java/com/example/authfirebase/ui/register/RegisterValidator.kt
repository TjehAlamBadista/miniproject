package com.example.authfirebase.ui.register

object RegisterValidator {

    private val existsingUser = listOf("Alambadista@gmail.com", "12345678")

    /**
     * Email dan Password tidak boleh kosong
     * Email harus valid
     * password harus lebih dari 8 karakter
     */

    fun registerValidator(
        email: String,
        password: String
    ) : Boolean{
        if (email.isEmpty() || password.isEmpty()){
            return false
        }
        if (email in existsingUser){
            return false
        }
        if (password.count { it.isDigit() } < 8 ){
            return false
        }
        return true
    }
}