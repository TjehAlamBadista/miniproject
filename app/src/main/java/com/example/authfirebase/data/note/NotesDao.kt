package com.example.authfirebase.data.note

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.authfirebase.data.note.NoteEntity

@Dao
interface NotesDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertNote(note : NoteEntity)

    @Update
    suspend fun updateNote(note: NoteEntity)

    @Delete
    suspend fun deleteNote(note: NoteEntity)

    @Query("Select * from notesTable order by id ASC")
    fun getAllNotes(): LiveData<List<NoteEntity>>

    @Query("SELECT COUNT(*) FROM notesTable")
    fun getCount(): Int
}