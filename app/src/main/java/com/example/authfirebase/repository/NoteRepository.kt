package com.example.authfirebase.repository

import androidx.lifecycle.LiveData
import com.example.authfirebase.data.note.NoteEntity
import com.example.authfirebase.data.note.NotesDao

open class NoteRepository(private val notesDao: NotesDao) {
    val allNotes: LiveData<List<NoteEntity>> = notesDao.getAllNotes()

    suspend fun insert(note: NoteEntity){
        notesDao.insertNote(note)
    }

    suspend fun delete(note: NoteEntity){
        notesDao.deleteNote(note)
    }

    suspend fun update(note: NoteEntity){
        notesDao.updateNote(note)
    }
}