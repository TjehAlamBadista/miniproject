package com.example.authfirebase.util

import android.view.View

interface IRecyclerClickListener {
    fun onItemClickListener(view: View?, position:Int)
}