package com.example.authfirebase.util;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
