package com.example.authfirebase.util

import com.example.authfirebase.ui.cart.model.CartModel

interface ICartLoadListener {
    fun onLoadCartSuccess(cartModelList: List<CartModel>)
    fun onLoadCartFailed(message:String?)
}