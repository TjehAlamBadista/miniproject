package com.example.authfirebase.util

import com.example.authfirebase.ui.cart.model.DrinkModel

interface IDrinkLoadListener {
    fun onDrinkLoadSuccess(drinkModelList: List<DrinkModel>)
    fun onDrinkLoadFailed(message:String?)
}