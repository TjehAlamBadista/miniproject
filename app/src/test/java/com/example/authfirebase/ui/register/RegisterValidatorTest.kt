package com.example.authfirebase.ui.register

import com.google.common.truth.Truth
import org.junit.Test

class RegisterValidatorTest {

    @Test
    fun EmailDanPasswordKosong(){
        val result = RegisterValidator.registerValidator(
            "",
            ""
        )

        Truth.assertThat(result).isFalse()
    }

    @Test
    fun EmailDanPasswordIsi(){
        val result = RegisterValidator.registerValidator(
            "badistaalam@gmail.com",
            "87654321"
        )

        Truth.assertThat(result).isTrue()
    }

    @Test
    fun EmailDanPasswordSudahAda(){
        val result = RegisterValidator.registerValidator(
            "Alambadista@gmail.com",
            "12345678"
        )

        Truth.assertThat(result).isFalse()
    }

    @Test
    fun PasswordKosong(){
        val result = RegisterValidator.registerValidator(
            "Alambadista@gmail.com",
            ""
        )

        Truth.assertThat(result).isFalse()
    }

    @Test
    fun PasswordKurangDariDelapan(){
        val result = RegisterValidator.registerValidator(
            "Alambadista@gmail.com",
            "12345"
        )

        Truth.assertThat(result).isFalse()
    }
}