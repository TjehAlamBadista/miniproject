package com.example.authfirebase.ui.login

import com.google.common.truth.Truth.assertThat
import org.junit.Test

class LoginValidatorTest{

    @Test
    fun EmailDanPasswordKosong(){
        val result = LoginValidator.inputValidator(
            "",
            ""
        )

        assertThat(result).isFalse()
    }

    @Test
    fun EmailDanPasswordBelumAda(){
        val result = LoginValidator.inputValidator(
            "badistaalam@gmail.com",
            "87654321"
        )

        assertThat(result).isFalse()
    }

    @Test
    fun EmailDanPasswordSudahAda(){
        val result = LoginValidator.inputValidator(
            "Alambadista@gmail.com",
            "12345678"
        )

        assertThat(result).isTrue()
    }

    @Test
    fun EmailKosong(){
        val result = LoginValidator.inputValidator(
            "",
            "12345678"
        )

        assertThat(result).isFalse()
    }

    @Test
    fun PasswordKosong(){
        val result = LoginValidator.inputValidator(
            "Alambadista@gmail.com",
            ""
        )

        assertThat(result).isFalse()
    }

    @Test
    fun PasswordKurangDariDelapan(){
        val result = LoginValidator.inputValidator(
            "Alambadista@gmail.com",
            "12345"
        )

        assertThat(result).isFalse()
    }
}