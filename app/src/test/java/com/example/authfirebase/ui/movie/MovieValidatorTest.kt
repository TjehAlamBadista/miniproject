package com.example.authfirebase.ui.movie

import com.example.authfirebase.ui.movie.model.Movie
import org.junit.Assert.assertEquals
import org.junit.Test

class MovieValidatorTest{

    @Test
    fun validateMovie(){
        val movie = Movie("1", "Hurig", "Alus Pokonamah", "Keisukan")
        assertEquals(true, MovieValidator.validateMovie(movie))
    }

    @Test
    fun validateMovieEmpty(){
        val movie = Movie("", "", "", "")
        assertEquals(false, MovieValidator.validateMovie(movie))
    }
}