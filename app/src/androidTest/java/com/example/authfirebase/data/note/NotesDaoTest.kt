package com.example.authfirebase.data.note

import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.filters.SmallTest
import com.example.authfirebase.ui.main.view.HomeFragment
import com.example.authfirebase.util.getOrAwaitValueAndroid
import com.example.authfirebase.util.launchFragmentInHiltContainer
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject
import javax.inject.Named

@ExperimentalCoroutinesApi
@SmallTest
@HiltAndroidTest
class NotesDaoTest {
    @get: Rule
    var hiltRule = HiltAndroidRule(this)

    @get: Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    @Named("noteTest_db")
    lateinit var database: NoteDatabase
    private lateinit var dao: NotesDao

    companion object{
        const val TAG: String = "Junit"
    }

    @Before
    fun setup(){
        hiltRule.inject()

        dao = database.getNotesDao()

        Log.i(TAG, "createDb")
    }

    @After
    fun teardown(){
        database.close()
        Log.i(TAG, "closeDb")
    }

//     Test Fragment
    @Test
    fun testLaunchFragmentHiltWithContainer(){
        launchFragmentInHiltContainer<HomeFragment> {

        }
    }

    @Test
    fun insertNote() = runBlockingTest {
        val note = NoteEntity("", "", "")
        note.id = 1
        dao.insertNote(note)

        val count: Int = dao.getCount()

        assertEquals(note.id, count)
    }

    @Test
    fun deleteNote() = runBlockingTest {
        val note = NoteEntity("", "", "")
        note.id = 1

        dao.insertNote(note)
        dao.deleteNote(note)

        val allNote = dao.getAllNotes().getOrAwaitValueAndroid()
        assertThat(allNote).doesNotContain(note)
    }
}