package com.example.authfirebase.data.movie

import com.google.gson.Gson
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MovieApiServiceTest{
    lateinit var mockWebServer: MockWebServer
    lateinit var apiService: MovieApiService
    lateinit var gson: Gson

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        gson = Gson()
        mockWebServer = MockWebServer()
        apiService = Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(MovieApiService::class.java)
    }

    @Test
    fun testApi(){
        runBlocking {
            val mockResponse = MockResponse()
            mockWebServer.enqueue(mockResponse.setBody("[]"))
            val response = apiService
            val request = mockWebServer.takeRequest()
            assertEquals("https://api.themoviedb.org", request.path)
            assertEquals(true, response)
        }
    }

//    @After
//    fun tearDown(){
//        mockWebServer.shutdown()
//    }
}