package com.example.authfirebase.data.note

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.example.authfirebase.util.getOrAwaitValue
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.*

@HiltAndroidTest
class NoteDatabaseTest:TestCase(){

    @get:Rule // <----
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var db: NoteDatabase
    private lateinit var dao: NotesDao

    @Before
    override fun setUp(){
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, NoteDatabase::class.java)
            .allowMainThreadQueries().build()

        dao = db.getNotesDao()
    }

    @After
    fun closeDb(){
        closeDb()
    }

    @Test
    fun testDatabase() = runBlocking {
        val note = NoteEntity("", "", "")
        note.id = 1
        dao.insertNote(note)

        val count: Int = dao.getCount()

        Assert.assertEquals(note.id, count)
    }
}