package com.example.authfirebase.ui.bottomsheet

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.example.authfirebase.R
import com.example.authfirebase.ui.bottomsheet.view.BottomSheetActivity
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Test

@HiltAndroidTest
class BottomSheetActivityTest{

    @Test
    fun testActivityVisibility(){
        val activityScenario = ActivityScenario.launch(BottomSheetActivity::class.java)
        Espresso.onView(ViewMatchers.withId(R.id.bottomSheet_Layout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(ViewActions.click())
    }

    @Test
    fun testButtonAndDialog(){
        val activityScenario = ActivityScenario.launch(BottomSheetActivity::class.java)
        Espresso.onView(ViewMatchers.withId(R.id.btnPresistent))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(ViewActions.click())

        val btnModal = ActivityScenario.launch(BottomSheetActivity::class.java)
        Espresso.onView(ViewMatchers.withId(R.id.btnModal))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.bottom_sheet_presistent))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(ViewActions.click())

        val btnDialog = ActivityScenario.launch(BottomSheetActivity::class.java)
        Espresso.onView(ViewMatchers.withId(R.id.btnDialog))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(ViewActions.click())

        val btnBottomDialog = ActivityScenario.launch(BottomSheetActivity::class.java)
        Espresso.onView(ViewMatchers.withId(R.id.btnBottomDialog))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(ViewActions.click())
    }

}