package com.example.authfirebase.ui.bottomsheet

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.example.authfirebase.R
import com.example.authfirebase.ui.bottomsheet.view.DialogAlertActivity
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Test

@HiltAndroidTest
class DialogAlertActivityTest{

    @Test
    fun setActivityVisibility(){
        val activityScenario = ActivityScenario.launch(DialogAlertActivity::class.java)
        Espresso.onView(withId(R.id.DialogAlert_Layout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(click())
    }

    @Test
    fun setDialogVisibility(){
        val btnShow = ActivityScenario.launch(DialogAlertActivity::class.java)
        Espresso.onView(withId(R.id.btnShow))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(click())

        Espresso.onView(withId(R.id.itemDialogAlert))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(click())
    }
}