package com.example.authfirebase.ui.bottomsheet

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.example.authfirebase.R
import com.example.authfirebase.ui.bottomsheet.view.DialogAlertActivity
import com.example.authfirebase.ui.bottomsheet.view.DialogBottomActivity
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Test

@HiltAndroidTest
class DialogBottomActivityTest{

    @Test
    fun testActivityVisibility(){
        val activityScenario = ActivityScenario.launch(DialogBottomActivity::class.java)
        Espresso.onView(ViewMatchers.withId(R.id.dialogBottom_Layout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(ViewActions.click())
    }

    @Test
    fun testButtonShow(){
        val activityScenario = ActivityScenario.launch(DialogAlertActivity::class.java)
        Espresso.onView(ViewMatchers.withId(R.id.btnShow))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
            .perform(ViewActions.click())

    }
}