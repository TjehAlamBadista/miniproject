package com.example.authfirebase.ui.view

import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.activityScenarioRule
import com.example.authfirebase.R
import com.example.authfirebase.ui.main.view.HomeActivity
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
class HomeActivityTest{

    @get:Rule
    var activityScenarioRule =  activityScenarioRule<HomeActivity>()

    @Test
    fun testActivityVisibility(){
        Espresso.onView(withId(R.id.HomeActivityLayout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun testBottomNavigationVisibility(){
        Espresso.onView(withId(R.id.HomeActivityLayout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(withId(R.id.menu_bottom))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun testFragmentVisibility(){
        Espresso.onView(withId(R.id.HomeActivityLayout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(withId(R.id.nav_host_fragment))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}