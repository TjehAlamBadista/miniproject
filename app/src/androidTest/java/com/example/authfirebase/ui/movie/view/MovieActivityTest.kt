package com.example.authfirebase.ui.movie.view

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import com.example.authfirebase.R
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MovieActivityTest {

    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MovieActivity::class.java)

    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
    }

    @Test
    fun testRecyclerView(){
        val activityScenario = ActivityScenario.launch(MovieActivity::class.java)
        Espresso.onView(withId(R.id.rv_movies_list)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))

//        Espresso.onView(ViewMatchers.withId(R.id.rv_movies_list)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
//
//        val nameItem = "Movie"
//        Espresso.onView(withText(nameItem)).check(matches(isDisplayed()))
    }

    @Test
    fun testAdapter(){
        val activityScenario = ActivityScenario.launch(MovieActivity::class.java)
        var recyclerView : RecyclerView = activityRule.activity.findViewById(R.id.rv_movies_list)
        var itemCount = recyclerView.adapter?.itemCount

        if (itemCount != null){
            Espresso.onView(withId(R.id.rv_movies_list)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(itemCount.minus(1)))
        }
    }
}