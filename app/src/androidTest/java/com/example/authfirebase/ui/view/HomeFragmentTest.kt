package com.example.authfirebase.ui.view

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragment
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.example.authfirebase.R
import com.example.authfirebase.ui.main.view.HomeFragment
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Test

@HiltAndroidTest
class HomeFragmentTest{

    private lateinit var scenario: FragmentScenario<HomeFragment>

    @Before
    fun setUp(){
        scenario = launchFragment()
        scenario.moveToState(Lifecycle.State.RESUMED)
    }

    @Test
    fun testFragmentHomeVisibility(){
        val scenario = launchFragmentInContainer<HomeFragment>()
        Espresso.onView(withId(R.id.HomeFragmentLayout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun testVisibilityButton(){
        val scenario = launchFragmentInContainer<HomeFragment>()
        Espresso.onView(withId(R.id.HomeFragmentLayout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(withId(R.id.btnNotifikasi))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(withId(R.id.btnMaps))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(withId(R.id.btnDrawMaps))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(withId(R.id.btnLiveChat))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(withId(R.id.btnBottomSheet))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(withId(R.id.btnDatePicker))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(withId(R.id.btnMovie))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(withId(R.id.btnKeranjang))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(withId(R.id.btnRating))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(withId(R.id.btnGesture))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(withId(R.id.btnTabLayout))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.onView(withId(R.id.btnNote))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun testClickButton(){
        val btnNofikasi = launchFragmentInContainer<HomeFragment>()
        Espresso.onView(withId(R.id.btnNotifikasi))
            .perform(click())

        val btnDrawMaps = launchFragmentInContainer<HomeFragment>()
        Espresso.onView(withId(R.id.btnDrawMaps))
            .perform(click())

        val btnLiveChat = launchFragmentInContainer<HomeFragment>()
        Espresso.onView(withId(R.id.btnLiveChat))
            .perform(click())

        val btnBottomSheet = launchFragmentInContainer<HomeFragment>()
        Espresso.onView(withId(R.id.btnBottomSheet))
            .perform(click())

        val btnDatePicker = launchFragmentInContainer<HomeFragment>()
        Espresso.onView(withId(R.id.btnDatePicker))
            .perform(click())

        val btnMovie = launchFragmentInContainer<HomeFragment>()
        Espresso.onView(withId(R.id.btnMovie))
            .perform(click())

        val btnKeranjang = launchFragmentInContainer<HomeFragment>()
        Espresso.onView(withId(R.id.btnKeranjang))
            .perform(click())

        val btnRating = launchFragmentInContainer<HomeFragment>()
        Espresso.onView(withId(R.id.btnRating))
            .perform(click())

        val btnGesture = launchFragmentInContainer<HomeFragment>()
        Espresso.onView(withId(R.id.btnGesture))
            .perform(click())

        val btnTabLayout = launchFragmentInContainer<HomeFragment>()
        Espresso.onView(withId(R.id.btnTabLayout))
            .perform(click())

        val btnNote = launchFragmentInContainer<HomeFragment>()
        Espresso.onView(withId(R.id.btnNote))
            .perform(click())
    }
}